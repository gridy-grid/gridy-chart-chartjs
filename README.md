# GridyGrid chart component ChartsJS driver

## Installation

```shell
npm i gridy-grid chart.js gridy-chart-chartjs gridy-grid-default sk-theme-default
```

and plug it to your page

```html
<script src="/node_modules/chart.js/dist/chart.js"></script>
```

then add element to your container or setup it programmatically

```html
    <gridy-grid id="gridyGrid" base-path="/node_modules/gridy-grid/src" sort-field="$.title">
        <gridy-data-source fields='[{ "title": "Price1", "path": "$.price1", "borderColor": "red", "backgroundColor": "green", "borderWidth": 1 },{ "title": "Price2", "path": "$.price2", "borderColor": "green", "backgroundColor": "yellow", "borderWidth": 1 }]'
                       datasource-type="DataSourceLocal" datapath="$.data"></gridy-data-source>
    
        <gridy-chart dri="chart-js" type="line" field-price1="Price1" field-price2="Price2" width="400" height="300" options='{"scales":{"y":{ "beginAtZero": true }}}'></gridy-chart>
    
    </gridy-grid>
    <script type="module">
        import { GridyGrid } from '/node_modules/gridy-grid/src/gridy-grid.js';
        let data = [];
        for (let i = 0; i < 10; i++) {
            data.push({ price1: 200 * i, price2: 100 * i })
        }
        let grid = document.querySelector('#gridyGrid');
        grid.addEventListener('bootstrap', () => {
            grid.charts[0].addEventListener('skrender', (event) => {
                grid.dataSource.loadData(data);
            });
        });
        customElements.define('gridy-grid', GridyGrid);
    </script>
    
```
