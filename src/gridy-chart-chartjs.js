import { GridyChartDriver } from "../../gridy-grid/src/chart/gridy-chart-driver.js";
import { FieldMappedChart } from "../../gridy-grid/src/chart/impl/field-mapped-chart.js";

import { JsonPath } from '../../sk-core/src/json-path.js';

export class DefaultChartJs extends FieldMappedChart {
    
    async fmtData(data) {
        let datasets = [];
        let min = 0, max = 0;
        for (let fieldKey of Object.keys(this.fieldMappings)) {
            let field = this.fieldMappings[fieldKey];
            let dsData = [];
            for (let dataItem of data) {
                let onValue = await JsonPath.query(dataItem, field.path);
                let value = onValue[0];
                if (value === undefined) {
                    value = dataItem;
                }
                if (! isNaN(value) && value < min) {
                    min = value;
                }
                if (! isNaN(value) && value > max) {
                    max = value;
                }
                dsData.push(value);
            }
            let dataSet = {
                label: field.title,
                data: dsData,
                fill: false,
                tension: 0.1
            };
            Object.assign(dataSet, field);
            datasets.push(dataSet);
        }
        let step = (max - min) / 4;
        return {
            labels: [ min, min + step, min + (2 * step), min + (3 * step), max ],
            datasets: datasets
        };
    }
    
    render(container, options) {
        let chart = new Chart(container, options);
        chart.render();
        return chart;    
    }
}

export class GridyChartChartJs extends GridyChartDriver {

    
    implByType(type) {				
        return new DefaultChartJs(this.el);
	}
    
	async renderChart(el, data) {

		this.type = el.getAttribute('type');

		this.chartImpl = this.implByType(this.type);
		this.chartImpl.configFromEl(el);

		let fmtedData = await this.chartImpl.fmtData(data);
        let container = el.querySelector('.gridy-chart-container');
        container.innerHTML = '';
        container.insertAdjacentHTML('beforeend', `<canvas width="${el.width}" height="${el.height}"></canvas>`);
        el.ctx = container.querySelector('canvas');
        let config = {
            type: el.chartType,
            data: fmtedData
        };
        if (el.options) {
            config.options = el.options;
        }
        
        this.chart = this.chartImpl.render(el.ctx, config);
        return Promise.resolve(this.chart);
    }
}
